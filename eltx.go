package eltex_olt

import (
	"errors"
	"fmt"
	g "github.com/soniah/gosnmp"
	"os"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"log"
)

type ONTState int

const (
	free ONTState = iota
	allocated
	authInProgress
	authFailed
	authOk
	cfgInProgress
	cfgFailed
	ok
	failed
	blocked
	mibreset
	preconfig
	fwUpdating
	unactivated
	redundant
	disabled
	unknown
)

var eltxPrefix = "69.76.84.88."

type SNMP struct {
	RWCommunity *string
	SNMP        *g.GoSNMP
}

type ONT struct {
	Serial     string
	State      int
	Channel    uint
	Id         uint
	EquipType  string
	TemplateId uint
	Description string
}

type Template struct {
	Id   string
	Name string
}

func Connect(host string, community *string, rwCommunity *string) (*SNMP, error) {
	s := SNMP{}
	g.Default.Target = host
	if community != nil {
		g.Default.Community = *community
	}
	if rwCommunity != nil {
		s.RWCommunity = rwCommunity
	}

	err := g.Default.Connect()
	if err != nil {
		return nil, err
	}

	s.SNMP = g.Default
	return &s, nil
}

func (s *SNMP) getBulkWalk(oid string, walkFunc g.WalkFunc) {
	if walkFunc == nil {
		walkFunc = defaultWalkFunc
	}

	err := s.SNMP.BulkWalk(oid, walkFunc)
	if err != nil {
		fmt.Printf("Walk Error: %v\n", err)
		os.Exit(1)
	}
}

func (s *SNMP) getSNMP(oid string, serial *string) (results []g.SnmpPDU, err error) {
        if serial != nil {
                result, err := converttodec(*serial)
                oid = oid + "." + result
                str := []string{}
                str = append(str, oid)
                r, err := s.SNMP.Get(str)
                if err != nil {
                        return results, err
                }
                results = r.Variables
        }

        if serial == nil {
                results, err = s.SNMP.BulkWalkAll(oid)
                if err != nil {
                        return results, err
                }
        }
        return results, nil

}


//state by serial
func (s *SNMP) GetONTState(serial *string) (serialStatus map[string]int, err error) {
	oid := "1.3.6.1.4.1.35265.1.22.3.1.1.5.1.8"
	results, err := s.getSNMP(oid, serial)
        if err != nil {
                return serialStatus, err
        }

	serialStatus = make(map[string]int)
	for _, v := range results {
		if v.Value == nil {
			return serialStatus, nil
		}
		_, hexserial := getSerialFromOid(v.Name)
		serialStatus[hexserial] = v.Value.(int)
	}
	return serialStatus, nil
}

//All ont with specified state
func (s *SNMP) GetONTByState(state int) (serialStatus map[string]int, err error) {
	oid := "1.3.6.1.4.1.35265.1.22.3.1.1.5.1.8"
	results, err := s.SNMP.BulkWalkAll(oid)
	if err != nil {
		return serialStatus, err
	}

	serialStatus = make(map[string]int)

	for _, v := range results {
		if v.Value.(int) == state {
			_, hexserial := getSerialFromOid(v.Name)
			serialStatus[hexserial] = v.Value.(int)
		}
	}
	return serialStatus, nil
}

//Return ONT serial and it channel
func (s *SNMP) GetONTStateChannel(serial *string) (serialChannel map[string]uint, err error) {
        oid := "1.3.6.1.4.1.35265.1.22.3.1.1.3.1.8"
        results, err := s.getSNMP(oid, serial)
        if err != nil {
                return serialChannel, err
        }

        serialChannel = make(map[string]uint)
        for _, v := range results {
                if v.Value == nil {
                        return serialChannel, nil
                }
                _, hexserial := getSerialFromOid(v.Name)
                serialChannel[hexserial] = v.Value.(uint)
        }
        return serialChannel, nil
}

//Return oid for all onts or for specific serial
func (s *SNMP) GetONTStateId(serial *string) (serialId map[string]uint, err error) {
        oid := "1.3.6.1.4.1.35265.1.22.3.1.1.4.1.8"
        results, err := s.getSNMP(oid, serial)
        if err != nil {
                return serialId, err
        }


        serialId = make(map[string]uint)
        for _, v := range results {
                if v.Value == nil {
                        return serialId, nil
                }
                _, hexserial := getSerialFromOid(v.Name)
                serialId[hexserial] = v.Value.(uint)
        }
        return serialId, nil
}

//Return ONT serial and it channel
func (s *SNMP) GetONTConfigChannel(serial *string) (serialChannel map[string]uint, err error) {
	oid := "1.3.6.1.4.1.35265.1.22.3.4.1.3.1.8"
	results, err := s.getSNMP(oid, serial)
        if err != nil {
                return serialChannel, err
        }

	serialChannel = make(map[string]uint)
	for _, v := range results {
		if v.Value == nil {
			return serialChannel, nil
		}
		_, hexserial := getSerialFromOid(v.Name)
		serialChannel[hexserial] = v.Value.(uint)
	}
	return serialChannel, nil
}

//Return ONT serial and it channel
func (s *SNMP) GetONTConfigRowStatus(serial *string) (serialStatus map[string]int, err error) {
        oid := "1.3.6.1.4.1.35265.1.22.3.4.1.20.1.8"
        results, err := s.getSNMP(oid, serial)
        if err != nil {
                return serialStatus, err
        }

        serialStatus = make(map[string]int)
        for _, v := range results {
                if v.Value == nil {
                        return serialStatus, nil
                }
                _, hexserial := getSerialFromOid(v.Name)
                serialStatus[hexserial] = v.Value.(int)
        }
        return serialStatus, nil
}


//Return oid for all onts or for specific serial
func (s *SNMP) GetONTConfigId(serial *string) (serialId map[string]uint, err error) {
	oid := "1.3.6.1.4.1.35265.1.22.3.4.1.4.1.8"
	results, err := s.getSNMP(oid, serial)
        if err != nil {
                return serialId, err
        }


	serialId = make(map[string]uint)
	for _, v := range results {
		if v.Value == nil {
			return serialId, nil
		}
		_, hexserial := getSerialFromOid(v.Name)
		serialId[hexserial] = v.Value.(uint)
	}
	return serialId, nil
}

func (s *SNMP) GetEquipmentType(serial *string) (equipType map[string]string, err error) {
	oid := "1.3.6.1.4.1.35265.1.22.3.1.1.12.1.8"
	results, err := s.getSNMP(oid, serial)
        if err != nil {
                return equipType, err
        }

	equipType = make(map[string]string)

	for _, v := range results {
		if v.Value == nil {
			return equipType, nil
		}
		_, hexserial := getSerialFromOid(v.Name)
		equipType[hexserial] = string([]byte(v.Value.([]uint8)))
	}
	return equipType, nil

}

func (s *SNMP) GetONTTemplate(serial *string) (template map[string]uint, err error) {
	oid := "1.3.6.1.4.1.35265.1.22.3.4.1.43.1.8"
	results, err := s.getSNMP(oid, serial)
        if err != nil {
                return template, err
        }


	template = make(map[string]uint)

	for _, v := range results {
		if v.Value == nil {
			return template, nil
		}
		_, hexserial := getSerialFromOid(v.Name)
		template[hexserial] = v.Value.(uint)
	}
	return template, nil

}


//Received signal strength indication. Measured in 0.1dbm.
//Value 65535 - means that RSSI value is not available (not supported by SFP)
func (s *SNMP) GetRSSI(serial *string) (rssi map[string]int, err error) {
	oid := "1.3.6.1.4.1.35265.1.22.3.1.1.11.1.8"
	results, err := s.getSNMP(oid, serial)
	if err != nil {
		return rssi, err
	}

	rssi = make(map[string]int)
	for _, v := range results {
		if v.Value == nil {
			return rssi, nil
		}
		_, hexserial := getSerialFromOid(v.Name)
		rssi[hexserial] = v.Value.(int)
	}
	return rssi, nil
}

//Get all ONT data at once for all onts
func (s *SNMP) GetAllONTData(serial *string) (serialData map[string]ONT, err error) {
	serialId, err := s.GetONTStateId(serial)
	if err != nil {
		return serialData, err
	}
	serialState, err := s.GetONTState(serial)
	if err != nil {
                return serialData, err
        }

	serialChannel, err := s.GetONTStateChannel(serial)
	if err != nil {
                return serialData, err
        }
	equipType, err := s.GetEquipmentType(serial)
	if err != nil {
                return serialData, err
        }
	template, err := s.GetONTTemplate(serial)
	if err != nil {
                return serialData, err
        }
	description, err := s.GetONTDescription(serial)
	if err != nil {
                return serialData, err
        }

	serialData = make(map[string]ONT)
	for k, _ := range serialId {
		serialData[k] = ONT{
			Serial:     k,
			State:      serialState[k],
			Channel:    serialChannel[k],
			Id:         serialId[k],
			EquipType:  equipType[k],
			TemplateId: template[k],
			Description: description[k],
		}
	}
	return serialData, nil
}

//All ont ids on specific channel
func (s *SNMP) GetONTIdForChannel(channel uint) (oids []uint, err error) {
	serialId, err := s.GetONTConfigId(nil)
	if err != nil {
		return oids, err
	}
	log.Printf("ELTX: Настроенные ontid для всех ont %+v\n", serialId)
	serialChannel, err := s.GetONTConfigChannel(nil)
	if err != nil {
                return oids, err
        }
	log.Printf("ELXT: Все Серийники и их каналы: %+v\n", serialChannel)

	for k, _ := range serialChannel {
		if serialChannel[k] == channel {
			if val, ok := serialId[k]; ok {
				oids = append(oids, val)
			}
		}
	}
	sort.Slice(oids, func(i, j int) bool { return oids[i] < oids[j] })
	return oids, nil
}

//Return first free ont id on specidic channel
func (s *SNMP) GetFreeOidOnChannel(channel uint) (freeOid int, err error) {
	oids, err := s.GetONTIdForChannel(channel)
	freeOid = -1
	if err != nil {
		return freeOid, err
	}

	//Если ничего не настроено на канале, то юзаем 0й id
	if len(oids) == 0 {
		freeOid = 0
	}
	log.Printf("ELTX: Настроенные ontid для канала %d: %+v\n", channel, oids)
	for i, v := range oids {
		if i == 127 {  //Максимальное число каналов
			return freeOid, errors.New("No Free oids on channel")
                }

                if i == 0 && v != 0 {
			freeOid = 0
			break
                }

                if i < int(v) {
                        freeOid = i
                        break
                }

		if i == (len(oids)-1) {
			freeOid = i + 1
			break
		}

        }

	return freeOid, nil
}

//Parse serial from oid
func getSerialFromOid(oid string) (stringoid string, stringhex string) {
	arr := strings.Split(oid, ".")
	ser := arr[len(arr)-4:]
	stringoid = eltxPrefix + strings.Join(ser, ".")
	stringhex = converttohex(ser)
	return stringoid, stringhex
}

func (s *SNMP) AddONT(ONT *ONT) (ok bool, err error) {
	if ONT.Id > 127 {
		data, err := s.GetFreeOidOnChannel(ONT.Channel)
		log.Printf("ELTX: Find free oid %d on channel %d for ONT %s\n", data, ONT.Channel, ONT.Serial)
		if err != nil {
			return false, err
		}
		ONT.Id = uint(data)
	}
	serialDec, _ := converttodec(ONT.Serial)
	// ROWSTATUS INTEGER {active(1), notInService(2), notReady(3), createAndGo(4), createAndWait(5), destroy(6) }
	configRowOid := "1.3.6.1.4.1.35265.1.22.3.4.1.20.1.8." + serialDec
	configChannelOid := "1.3.6.1.4.1.35265.1.22.3.4.1.3.1.8." + serialDec
	configONTIdOid := "1.3.6.1.4.1.35265.1.22.3.4.1.4.1.8." + serialDec

	pdus := []g.SnmpPDU{
		{Name: configRowOid,
			Type:  g.Integer,
			Value: 4,
		},
		{Name: configChannelOid,
			Type:  g.Gauge32,
			Value: uint32(ONT.Channel),
		},
		{Name: configONTIdOid,
			Type:  g.Gauge32,
			Value: uint32(ONT.Id),
		},
	}
	log.Printf("ELTX: Настроена на канал %d id %d для онт %s\n", uint32(ONT.Channel),  uint32(ONT.Id), ONT.Serial)

	return snmpSet(s, pdus)
}

func restoreCommunity(s *SNMP, currentCommunity string) *SNMP {
	s.SNMP.Community = currentCommunity
	return s
}

//snmpset -v2c -c private 192.168.0.1 ltp8xONTConfigRowStatus.1.8.69.76.84.88.36.168.0.18 i 6
func (s *SNMP) DeleteONT(serial string) (ok bool, err error) {
	configRowOid := "1.3.6.1.4.1.35265.1.22.3.4.1.20.1.8."
	result, err := converttodec(serial)
	if err != nil {
		return false, err
	}

	pdus := []g.SnmpPDU{
		{Name: configRowOid + result,
			Type:  g.Integer,
			Value: 6,
		},
	}
	return snmpSet(s, pdus)
}

func checkSNMPRespond(result *g.SnmpPacket) (err error) {
	if result.ErrorIndex > 0 {
		return errors.New("SNMP Query Error: " + result.Error.String())
	}

	return nil
}

//Value 65535 means that template is not assigned.
func (s *SNMP) MapONTtoTemplate(serial string, template uint) (ok bool, err error) {
	configOid := "1.3.6.1.4.1.35265.1.22.3.4.1.43.1.8."

	result, err := converttodec(serial)
	if err != nil {
		return false, err
	}
	pdus := []g.SnmpPDU{
		{Name: configOid + result,
			Type:  g.Gauge32,
			Value: uint32(template),
		},
	}

	return snmpSet(s, pdus)
}

func (s *SNMP) GetONTDescription(serial *string) (description map[string]string, err error) {
        oid := "1.3.6.1.4.1.35265.1.22.3.4.1.8.1.8"
        results, err := s.getSNMP(oid, serial)
        if err != nil {
                return description, err
        }

        description = make(map[string]string)

        for _, v := range results {
                if v.Value == nil {
                        return description, nil
                }
                _, hexserial := getSerialFromOid(v.Name)
                description[hexserial] = string(v.Value.([]uint8))
        }
        return description, nil

}

func (s *SNMP) SetONTDescription(serial string, descr string) (ok bool, err error) {
        configOid := "1.3.6.1.4.1.35265.1.22.3.4.1.8.1.8."

        result, err := converttodec(serial)
        if err != nil {
                return false, err
        }
        pdus := []g.SnmpPDU{
                {Name: configOid + result,
                        Type:  g.OctetString,
                        Value: descr,
                },
        }

        return snmpSet(s, pdus)
}


func snmpSet(s *SNMP, pdus []g.SnmpPDU) (ok bool, err error) {
	currentCommunity := s.SNMP.Community
	s.SNMP.Community = *s.RWCommunity
	defer restoreCommunity(s, currentCommunity)
	res, err := s.SNMP.Set(pdus)
	if err != nil {
		return false, err
	}
	if err := checkSNMPRespond(res); err != nil {
		return false, err
	}
	return true, nil
}

func (s *SNMP) SaveConfig() (ok bool, err error) {
	saveOid := "1.3.6.1.4.1.35265.1.22.1.50.0"
	pdus := []g.SnmpPDU{
                {Name: saveOid,
                        Type:  g.Gauge32,
                        Value: uint32(1),
                },
        }
	return snmpSet(s, pdus)
}

func (s *SNMP) GetTemplates() (templates []Template, err error) {
	oid := "1.3.6.1.4.1.35265.1.22.3.24.1.1.2"

	results, err := s.SNMP.BulkWalkAll(oid)
	if err != nil {
		return templates, err
	}

	for _, v := range results {
		if v.Value == nil {
			return templates, nil
		}
		arr := strings.Split(v.Name, ".")
		ser := arr[len(arr)-1:]
		templates = append(templates, Template{Id: ser[0], Name: string(v.Value.([]uint8))})
	}

	return templates, nil
}

/*snmpset -v2c -c private 192.168.0.1 ltp8xONTDisableONTSerial.0 x
  "454C545824A80012" ltp8xONTDisableChannel.0 u 6
  ltp8xONTDisableActionDisable.0 u 1
*/
func (s *SNMP) DisableONT(ONT *ONT) (ok bool, err error) {
	return true, nil
}

func converttohex(array []string) (result string) {
	for _, v := range array {
		v, _ := strconv.ParseInt(v, 10, 10)
		change := strconv.FormatInt(v, 16)
		//добиваем до двух знаков, если длина меньше двух
		if len(change) < 2 {
			change = "0" + change
		}
		result = result + change
	}
	return strings.ToUpper(result)
}

func converttodec(serial string) (result string, err error) {
	if len(serial) != 8 {
		return "", errors.New("too short")
	}

	re := regexp.MustCompile(`(\S{2})`)
	x := re.FindAllString(serial, -1)

	dec := []string{}
	for _, v := range x {
		c, _ := strconv.ParseInt(v, 16, 0)
		change := strconv.FormatInt(c, 10)
		dec = append(dec, change)
	}

	result = eltxPrefix + strings.Join(dec, ".")
	return result, nil
}

func defaultWalkFunc(pdu g.SnmpPDU) error {
	fmt.Printf("%s = ", pdu.Name)

	switch pdu.Type {
	case g.OctetString:
		b := pdu.Value.([]byte)
		fmt.Printf("STRING: %s\n", string(b))
	default:
		fmt.Printf("TYPE %d: %d\n", pdu.Type, g.ToBigInt(pdu.Value))
	}
	return nil
}
